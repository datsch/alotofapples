package;

class GreenApple extends Apple {
    public function new() {
        super(AssetPaths.green_apple__png);
    }
}