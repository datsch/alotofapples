package;

import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.FlxState;
import flixel.FlxG;
import flixel.util.FlxColor;

class Credits2State extends FlxState{
    private var manual_text:FlxText;
    private var dedication_text:FlxText;
    private var tree_texture:FlxSprite;

    override public function create():Void {
        super.create();

        tree_texture = new FlxSprite((FlxG.width/2), (FlxG.height/3), AssetPaths.tree_spawnpoint__png);
        tree_texture.setGraphicSize(128,128);

        add(tree_texture);

        dedication_text = new FlxText(0, 8, FlxG.width, "\n"
            + "\nThis game was made for the Quarantine Jam in 2020 and is dedicated to all the hard-working medical personnel.\n"
            + "\n\nThank You!\n");
        dedication_text.setFormat(null, 16, FlxColor.WHITE, FlxTextAlign.CENTER);
        add(dedication_text);

        manual_text= new FlxText(0, FlxG.height - 48, FlxG.width, "Press 'Enter' for next page\nPress 'ESC' to return");
        manual_text.setFormat(null, 12, FlxColor.WHITE, FlxTextAlign.CENTER);

        add(manual_text);
    }

    override public function update(elapsed:Float):Void {
        super.update(elapsed);
        if(FlxG.keys.justPressed.ESCAPE){
            FlxG.camera.fade(FlxColor.BLACK, .33, false, function (){
                FlxG.switchState(new TitleState());
            });
        }
        if(FlxG.keys.justPressed.ENTER){
            FlxG.camera.fade(FlxColor.BLACK, .33, false, function (){
                FlxG.switchState(new Credits3State());
            });
        }
    }
}