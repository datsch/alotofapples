package;

import flixel.util.FlxTimer;
import flixel.FlxSubState;
import flixel.tile.FlxTilemap;
import flixel.group.FlxGroup;
import flixel.system.FlxSound;
import flixel.util.FlxColor;
import flixel.text.FlxText;
import flixel.FlxObject;
import flixel.FlxG;
import flixel.FlxState;

class PlayState extends FlxState
{
	var theDoctor:Doctor;
	var floor:Floor;
	var walls:FlxTilemap;
	var firstSpawnPoint:SpawnPoint;
	var secondSpawnPoint:SpawnPoint;
	var thirdSpawnPoint:SpawnPoint;
	var fourthSpawnPoint: SpawnPoint;

	var timeText:FlxText;
	var hitPointsText:FlxText;

	var appleHitSound:FlxSound;
	var doctorHitSound:FlxSound;

	public var enemies:FlxTypedGroup<Apple>;
	public var bullets:FlxTypedGroup<Bullet>;

	override public function create():Void
	{
		GameVariables.time = 0.0;
		GameVariables.applesToProduce = 1;
		GameVariables.nextProduceTime = 5;
		FlxG.fixedTimestep = false;
		super.create();
		floor = new Floor();
		add(floor);
		walls = WallsTilemapFactory.createWalls();
		add(walls);

		enemies = new FlxTypedGroup(5000);
		bullets = new FlxTypedGroup(5000);

		firstSpawnPoint = new SpawnPoint(5, this.enemies, "red");
		firstSpawnPoint.positionAt(FlxG.width/2-firstSpawnPoint.width/2, 10);
		secondSpawnPoint = new SpawnPoint(5+GameVariables.timeOfRound, this.enemies, "green");
		secondSpawnPoint.positionAt(FlxG.width/2-firstSpawnPoint.width/2, FlxG.height - 10 - secondSpawnPoint.height);
		thirdSpawnPoint = new SpawnPoint(5+2*GameVariables.timeOfRound, this.enemies, "yellow");
		thirdSpawnPoint.positionAt(FlxG.width/2 + 107 - thirdSpawnPoint.width/2, FlxG.height/2 - thirdSpawnPoint.height/2 + 2);
		fourthSpawnPoint = new SpawnPoint(5+3*GameVariables.timeOfRound, this.enemies, "blue");
		fourthSpawnPoint.positionAt(FlxG.width/2 - 107 - fourthSpawnPoint.width/2, FlxG.height/2 - fourthSpawnPoint.height/2 + 2);
		add(firstSpawnPoint);
		add(secondSpawnPoint);
		add(thirdSpawnPoint);
		add(fourthSpawnPoint);
		add(enemies);
		theDoctor = new Doctor(this.bullets);
		theDoctor.positionInCenter();
		add(theDoctor);
		add(bullets);

		this.timeText = new FlxText(0,0, FlxG.width, 'Placeholder', 8);
		updateTimeText();
		this.timeText.setFormat(null, 8, FlxColor.WHITE, FlxTextAlign.RIGHT);

		add(this.timeText);

		this.hitPointsText = new FlxText(0,0, FlxG.width, 'Placeholder', 8);
		updateHitPointsText();
		this.hitPointsText.setFormat(null, 8, FlxColor.WHITE, FlxTextAlign.LEFT);

		add(this.hitPointsText);

		if (FlxG.sound.music == null){
			FlxG.sound.play(AssetPaths.music__ogg, 0.5, true);
		}

		this.appleHitSound = FlxG.sound.load(AssetPaths.apple_hit__wav);
		this.doctorHitSound = FlxG.sound.load(AssetPaths.doctor_hit__wav);
	}

	override public function update(elapsed:Float):Void
	{
		GameVariables.time += elapsed;
		super.update(elapsed);
		FlxG.collide(theDoctor, floor);
		FlxG.collide(theDoctor, walls);

		bullets.forEach(function (bullet:Bullet) {
			FlxG.collide(walls, bullet, function (_, bullet:Bullet) {bullets.remove(bullet, true).destroy();});
			FlxG.collide(floor, bullet, function (_, bullet:Bullet) {bullets.remove(bullet, true).destroy();});
		});			

		enemies.forEach(function (enemy:Apple) {
				enemy.move(elapsed);
				bullets.forEach(function(bullet:Bullet) {
					FlxG.collide(enemy, bullet, function (enemy:Apple, bullet:Bullet) {
						enemies.remove(enemy, true).destroy();
						bullets.remove(bullet);
						bullet.destroy();
						this.appleHitSound.play();
					});
				});
				FlxG.collide(theDoctor, enemy, function (doc:Doctor, enemy:Apple) {
					enemies.remove(enemy, true).destroy();
					doc.hit();
					this.doctorHitSound.play();
					FlxG.camera.shake(0.01, 0.2);
					FlxG.camera.fade(FlxColor.RED, 0.2, true);
				});
				FlxG.collide(enemy, enemies, collideEnemyBounce);
				FlxG.collide(enemy, walls, collideEnemyBounce);
				FlxG.collide(enemy, floor, collideEnemyBounce);
		});
		
		updateTimeText();
		updateHitPointsText();
		updateRound();
		if(theDoctor.dead()) {
			new FlxTimer().start(0.5, function(Timer:FlxTimer):Void { this.visible = false; });
			FlxG.camera.fade(FlxColor.BLACK, 1.0, false, function (){
				FlxG.switchState(new GameOverState());
			});
		}
	}

	public function collideEnemyBounce(enemy:Apple, obj:FlxObject) {enemy.new_movement();}
	
	private function updateTimeText() {
		var formatTime:Int = Math.floor(GameVariables.time);
		this.timeText.text = 'Time\n $formatTime';
	}

	private function setInvisible() {
		
	}

	private function updateHitPointsText() {
		var formatText:String ="HP\n";
		for (i in 0...this.theDoctor.lives) {
			formatText += "+";
		}
		this.hitPointsText.text = formatText;
	}

	private function updateRound() {
		if (GameVariables.time > GameVariables.nextProduceTime) {
			updateNextProduceTime();
			updateAmountOfApples();
		}	
	}

	private function updateNextProduceTime() {
		GameVariables.nextProduceTime = GameVariables.nextProduceTime + GameVariables.timeOfRound;
	}

	private function updateAmountOfApples() {
		GameVariables.applesToProduce = GameVariables.applesToProduce + 1;
	}
}
