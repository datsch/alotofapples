package;

import flixel.group.FlxGroup;
import flixel.FlxSprite;

class SpawnPoint extends FlxSprite {
    public var isActive(default, null):Bool = false;
    private var _isGraphicChange:Bool = false;
    private var _starttime:Float;
    private var _enemyGroup:FlxTypedGroup<Apple>;
    private var _kindOfApple:String;

    override public function new(timedelta:Float, enemyGroup:FlxTypedGroup<Apple>, kindOfApple:String="red") {
        super();
        this.loadGraphic(AssetPaths.tree_spawnpoint__png);
        _starttime = GameVariables.time + timedelta;
        _enemyGroup = enemyGroup;
        _kindOfApple = kindOfApple;
    }

    override public function update(elapsed:Float) {
        super.update(elapsed);
        if (!isActive && GameVariables.time > _starttime) {
            activate();
            changeGraphic();
        }
        if (isActive && GameVariables.time > GameVariables.nextProduceTime) {
            produceApple();
        }
    }

    public function produceApple() {
        for (_ in 0...GameVariables.applesToProduce) {
            var apple = AppleFactory.createApple(_kindOfApple);
            apple.positionAt(x, y);
            _enemyGroup.add(apple);
        }
    }

    public function positionAt(x:Float, y:Float) {
        this.x = x;
        this.y = y;
    }

    public function activate() {
        isActive = true;
        _isGraphicChange = true;
    }

    private function changeGraphic() {
        if (_isGraphicChange) {
            if (isActive) {
                this.loadGraphic(AssetPaths.tree_spawnpoint_active__png);
            } else {
                this.loadGraphic(AssetPaths.tree_spawnpoint__png);
            }
            _isGraphicChange = false;
        }
    }
}