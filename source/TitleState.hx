package;

import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.FlxState;
import flixel.FlxG;
import flixel.util.FlxColor;

class TitleState extends FlxState {
    private var game_name:FlxText;
    private var sub_text:FlxText;
    private var manual_text:FlxText;
    private var credits_text:FlxText;

    private var red_apple_texture:FlxSprite;
    private var red_apple_texture2:FlxSprite;
    private var tree_texture:FlxSprite;

    override public function create():Void {
        super.create();


        red_apple_texture = new FlxSprite(FlxG.width - 48, 48, AssetPaths.red_apple__png);
        red_apple_texture.setGraphicSize(32,32);

        add(red_apple_texture);

        red_apple_texture2 = new FlxSprite(36, 48, AssetPaths.red_apple__png);
        red_apple_texture2.setGraphicSize(32,32);

        add(red_apple_texture2);

        tree_texture = new FlxSprite((FlxG.width/2), (FlxG.height/3), AssetPaths.tree_spawnpoint_active__png);
        tree_texture.setGraphicSize(128,128);

        add(tree_texture);

        game_name = new FlxText(0, 24, FlxG.width,"A lot of apples", 28);
        game_name.setFormat(null, 28, FlxColor.WHITE, FlxTextAlign.CENTER);
       
        add(game_name);

        sub_text = new FlxText(0, FlxG.height - 32, FlxG.width,"Press 'SPACE' to start", 12);
        sub_text.setFormat(null, 12, FlxColor.WHITE, FlxTextAlign.CENTER);
        
        add(sub_text);

        manual_text= new FlxText(0, FlxG.height/2 - 12, FlxG.width, "'WASD' to move\n'ARROW KEYS' to shoot", 12);
        manual_text.setFormat(null, 12, FlxColor.WHITE, FlxTextAlign.CENTER);

        add(manual_text);

        credits_text= new FlxText(0, FlxG.height/2 + 24, FlxG.width, "Press 'C' to see the credits", 12);
        credits_text.setFormat(null, 12, FlxColor.WHITE, FlxTextAlign.CENTER);

        add(credits_text);

    }

    override public function update(elapsed:Float):Void {
        super.update(elapsed);
        if(FlxG.keys.justPressed.SPACE){
            FlxG.camera.fade(FlxColor.BLACK, 1.0, false, function (){
                FlxG.switchState(new StoryState());
            });
        }
        if(FlxG.keys.justPressed.C){
            FlxG.camera.fade(FlxColor.BLACK, .33, false, function (){
                FlxG.switchState(new Credits1State());
            });
        }
    }
}