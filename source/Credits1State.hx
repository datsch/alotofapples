package;

import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.FlxState;
import flixel.FlxG;
import flixel.util.FlxColor;

class Credits1State extends FlxState{
    private var georgBeckerText:FlxText;
    private var davidTscheppenText:FlxText;
    private var manual_text:FlxText;
    private var libraries_text:FlxText;

    private var tree_texture:FlxSprite;

    override public function create():Void {
        super.create();
    
        tree_texture = new FlxSprite((FlxG.width/2), (FlxG.height/3), AssetPaths.tree_spawnpoint__png);
        tree_texture.setGraphicSize(128,128);

        add(tree_texture);

        georgBeckerText = new FlxText(0, 32, FlxG.width, "Georg Becker\n Game Design, Pixelart, Programming",12);
        georgBeckerText.setFormat(null, 16, FlxColor.WHITE, FlxTextAlign.CENTER);

        add(georgBeckerText);

        davidTscheppenText = new FlxText(0, 110, FlxG.width, "David Tscheppen\n Programming, Game Design",12);
        davidTscheppenText.setFormat(null, 16, FlxColor.WHITE, FlxTextAlign.CENTER);

        add(davidTscheppenText);

        manual_text= new FlxText(0, FlxG.height - 48, FlxG.width, "Press 'Enter' for next page\nPress 'ESC' to return");
        manual_text.setFormat(null, 12, FlxColor.WHITE, FlxTextAlign.CENTER);

        add(manual_text);
    }

    override public function update(elapsed:Float):Void {
        super.update(elapsed);
        if(FlxG.keys.justPressed.ESCAPE){
            FlxG.camera.fade(FlxColor.BLACK, .33, false, function (){
                FlxG.switchState(new TitleState());
            });
        }
        if(FlxG.keys.justPressed.ENTER){
            FlxG.camera.fade(FlxColor.BLACK, .33, false, function (){
                FlxG.switchState(new Credits2State());
            });
        }
    }
}