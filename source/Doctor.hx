package;

import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.system.FlxSound;
import flixel.FlxObject;
import flixel.FlxG;
import flixel.FlxSprite;
import Math.PI;
import Math.cos;

class Doctor extends FlxSprite {
    private var _viewControl: IsViewControllable;
    private var _moveControl: IsMovementControllable;
    public var lives: Int;
    private var _bulletGroup:FlxTypedGroup<Bullet>;

    private var _sounds:Array<FlxSound>;

    override public function new(bulletGroup:FlxTypedGroup<Bullet>) {
        super();
        this.loadGraphic(AssetPaths.doctor_down__png);
        this._viewControl = new ViewKeyControl();
        this._moveControl = new MovementKeyControl();
        this.lives = 3;

        this._sounds = [
            FlxG.sound.load(AssetPaths.high_bleep1__wav),
            FlxG.sound.load(AssetPaths.high_bleep2__wav)
        ];

        _bulletGroup = bulletGroup;
    }

    override public function graphicLoaded() {
        super.graphicLoaded();
        this.setSize(4, 8);
        this.centerOffsets();
    }

    override public function update(elapsed:Float) {
        super.update(elapsed);
        this.view();
        this.move(elapsed);
        
        if (isShoot()) {
            shoot();
        }
    }

    public function positionInCenter() {
        this.x = FlxG.width/2 - this.width/2;
        this.y = FlxG.height/2- this.height/2;
    }

    public function view() {
        if (_viewControl.isViewLeft()) {
            this.loadGraphic(AssetPaths.doctor_left__png);
            this.facing = FlxObject.LEFT;
        } else if (_viewControl.isViewRight()) {
            this.loadGraphic(AssetPaths.doctor_right__png);
            this.facing = FlxObject.RIGHT;
        } else if (_viewControl.isViewUp()) {
            this.loadGraphic(AssetPaths.doctor_up__png);
            this.facing = FlxObject.UP;
        } else if (_viewControl.isViewDown()) {
            this.loadGraphic(AssetPaths.doctor_down__png);
            this.facing = FlxObject.DOWN;
        }
    }

    public function move(elapsed: Float) {
        var diagFactor = _moveControl.isValidMovementCombo() ? cos(PI/4) : 1.0;
        var posDelta = 60*elapsed;
        if (_moveControl.isMoveLeft()) {   
            this.x = this.x - posDelta*diagFactor;
        }
        if (_moveControl.isMoveRight()) {
            this.x = this.x + posDelta*diagFactor;
        }
        if (_moveControl.isMoveUp()) {
            this.y = this.y - posDelta*diagFactor;
        }
        if (_moveControl.isMoveDown()) {
            this.y = this.y + posDelta*diagFactor;
        }
    }

    public function hit() {
        this.lives--;
    }

    public function dead():Bool {
        return this.lives <= 0;
    }

    public function shoot() {
        var bullet = new Bullet(this.facing);
        bullet.x = this.x;
        bullet.y = this.y;
        _bulletGroup.add(bullet);
        _sounds[0].play();
    }

    private function isShoot() {
        return FlxG.keys.justPressed.LEFT 
            || FlxG.keys.justPressed.RIGHT 
            || FlxG.keys.justPressed.UP 
            || FlxG.keys.justPressed.DOWN;
    }
}