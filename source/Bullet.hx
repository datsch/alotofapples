package;

import flixel.FlxObject;
import flixel.util.FlxColor;
import flixel.FlxSprite;

class Bullet extends FlxSprite {
    private var _direction:Int;

    override public function new(direction:Int) {
        super();
        this.makeGraphic(1, 1, FlxColor.WHITE);
        this.centerOrigin();
        this.setSize(1,1);
        _direction = direction;
    }

    override public function update(elapsed:Float) {
        super.update(elapsed);
        move(elapsed);
    }

    public function move(elapsed:Float) {
        var posDelta = 60*elapsed*1.2;
        if (_direction == FlxObject.LEFT) {
            this.x = this.x - posDelta;
        } else if (_direction == FlxObject.RIGHT) {
            this.x = this.x + posDelta;
        } else if (_direction == FlxObject.UP) {
            this.y = this.y - posDelta;
        } else if (_direction == FlxObject.DOWN) {
            this.y = this.y + posDelta;
        }
    }
}