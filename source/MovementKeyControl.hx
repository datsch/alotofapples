package;

import flixel.FlxG;

class MovementKeyControl implements IsMovementControllable {
    public function new() {}

    public function isMoveLeft() {return FlxG.keys.pressed.A;}

    public function isMoveRight() {return FlxG.keys.pressed.D;}

    public function isMoveUp() {return FlxG.keys.pressed.W;}

    public function isMoveDown() {return FlxG.keys.pressed.S;}

    public function isValidMovementCombo() {
        return (isMoveLeft() && isMoveUp()) 
            || (isMoveLeft() && isMoveDown())
            || (isMoveRight() && isMoveUp())
            || (isMoveRight() && isMoveDown());
    }
}