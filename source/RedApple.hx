package;

class RedApple extends Apple {
    public function new() {
        super(AssetPaths.red_apple__png);
    }
}