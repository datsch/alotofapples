package;

import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.FlxState;
import flixel.FlxG;
import flixel.util.FlxColor;

class Credits3State extends FlxState{
    private var manual_text:FlxText;
    private var libraries_text:FlxText;
    private var heading_text:FlxText;
    private var tree_texture:FlxSprite;

    override public function create():Void {
        super.create();

        tree_texture = new FlxSprite((FlxG.width/2), (FlxG.height/3), AssetPaths.tree_spawnpoint__png);
        tree_texture.setGraphicSize(128,128);

        add(tree_texture);

        heading_text = new FlxText(0, 8, FlxG.width, "Libraries used:\n");
        heading_text.setFormat(null, 12, FlxColor.WHITE, FlxTextAlign.CENTER);
        add(heading_text);

        libraries_text = new FlxText(0, 24, FlxG.width, "\n"
            + "\n The Haxe Standard Library (MIT License): Copyright (C)2005-2016 Haxe Foundation\n"
            + "\n hscript (MIT License): Copyright (C)2008-2017 Haxe Foundation\n"
            + "\n flixel (MIT License): Copyright (c) 2009 Adam 'Atomic' Saltsman, Copyright (c) 2012 Matt Tuttle, Copyright (c) 2013 HaxeFlixel Team\n"
            + "\n flixel-addons (MIT License): Copyright (c) 2018 HaxeFlixel Team\n"
            + "\n openfl (MIT License): Copyright (c) 2013-2019 Joshua Granick and other OpenFL contributors\n"
            + "\n lime (MIT License): Copyright (c) 2013-2020 Joshua Granick and other Lime contributors\n");
        libraries_text.setFormat(null, 8, FlxColor.WHITE, FlxTextAlign.CENTER);
        add(libraries_text);

        manual_text= new FlxText(0, FlxG.height - 32, FlxG.width, "Press 'ESC' to return");
        manual_text.setFormat(null, 12, FlxColor.WHITE, FlxTextAlign.CENTER);

        add(manual_text);
    }

    override public function update(elapsed:Float):Void {
        super.update(elapsed);
        if(FlxG.keys.justPressed.ESCAPE){
            FlxG.camera.fade(FlxColor.BLACK, .33, false, function (){
                FlxG.switchState(new TitleState());
            });
        }
    }
}