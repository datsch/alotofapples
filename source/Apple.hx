package;

import flixel.math.FlxRandom;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.math.FlxVector;
import flixel.FlxSprite;

class Apple extends FlxSprite {

    private var direction:FlxVector;
    private var speed:Float;

    override public function new(graphicAsset:FlxGraphicAsset) {
        super();
        this.new_movement();
        this.loadGraphic(graphicAsset);  
    }

    override public function graphicLoaded() {
        super.graphicLoaded();
        this.setSize(7,7);
        this.centerOrigin();
    }

    public function positionAt(x:Float, y:Float) {
        this.x = x;
        this.y = y;
    }

    public function move(elapsed:Float) {
        var movement_vector:FlxVector = this.direction.normalize();
        this.x += movement_vector.x * this.speed * elapsed;
        this.y += movement_vector.y * this.speed * elapsed;
    }
    private function random_dir() {
        var rng = new FlxRandom();
        this.direction = new FlxVector(rng.float(-1.0,1.0), rng.float(-1.0,1.0));
    }
    private function random_speed() {
        var rng = new FlxRandom();
        this.speed = rng.float(30.0, 100.0);
    }

    public function new_movement() {
        this.random_dir();
        this.random_speed();
    }
}