package;

class YellowApple extends Apple {
    public function new() {
        super(AssetPaths.yellow_apple__png);
    }
}