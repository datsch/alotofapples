package;

import flixel.math.FlxRandom;
import flixel.tile.FlxTilemap;
import flixel.FlxG;

class Floor extends FlxTilemap {
    override public function new() {
        super();
        this.loadMapFromCSV(
            AssetPaths.floor_source_map__txt, AssetPaths.floor_tile__png, 8, 8, null, 0, 0, 3);
        this.mixFloorTiles();
        this.y = this.y - 8;
        this.x = (FlxG.width - this.width)/2;
    }

    private function mixFloorTiles() {
        var rnd = new FlxRandom();
        for (i in 0..._data.length) {
            if (_data[i] < 3) {
                _data[i] = rnd.weightedPick([90, 5, 5]);
            }
        }
    }
}