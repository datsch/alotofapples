package;

import flixel.FlxG;

class ViewKeyControl implements IsViewControllable {
    public function new() {}
    
    public function isViewLeft():Bool {return FlxG.keys.pressed.LEFT;}
    public function isViewRight():Bool {return FlxG.keys.pressed.RIGHT;}
    public function isViewUp():Bool {return FlxG.keys.pressed.UP;}
    public function isViewDown():Bool {return FlxG.keys.pressed.DOWN;}
}