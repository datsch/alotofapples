package;

import flixel.text.FlxText;
import flixel.FlxState;
import flixel.FlxG;
import flixel.util.FlxColor;
import Math.floor;

class GameOverState extends FlxState {
    
    private var time:Float;
    
    private var gameOverText:FlxText;
    private var scoreDisplayText:FlxText;
    private var manual_text:FlxText;

    override public function create():Void {
        super.create();
        this.time = GameVariables.time;

        gameOverText = new FlxText(0, 24, FlxG.width,"GAME OVER", 28);
        gameOverText.setFormat(null, 28, FlxColor.WHITE, FlxTextAlign.CENTER);

        add(gameOverText);

        scoreDisplayText = new FlxText(0, FlxG.height - 32, FlxG.width,"Congratulations, you survived " + floor(time) + " seconds!", 12);
        scoreDisplayText.setFormat(null, 12, FlxColor.WHITE, FlxTextAlign.CENTER);

        add(scoreDisplayText);

        manual_text= new FlxText(0, FlxG.height/2, FlxG.width, "Press 'R' to restart\nPress 'C' to see credits", 12);
        manual_text.setFormat(null, 12, FlxColor.WHITE, FlxTextAlign.CENTER);

        add(manual_text);
    }

    override public function update(elapsed:Float):Void {
        super.update(elapsed);
        if(FlxG.keys.justPressed.R){
            FlxG.camera.fade(FlxColor.BLACK, 1.0, false, function (){
                FlxG.switchState(new PlayState());
            });
        }
        if(FlxG.keys.justPressed.C){
            FlxG.camera.fade(FlxColor.BLACK, .33, false, function (){
                FlxG.switchState(new Credits1State());
            });
        }
    }
}