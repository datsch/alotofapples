package;

import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.FlxState;
import flixel.FlxG;
import flixel.util.FlxColor;

class StoryState extends FlxState {
    private var game_name:FlxText;
    private var sub_text:FlxText;
    private var manual_text:FlxText;
    private var credits_text:FlxText;
    
    private var doctor_texture:FlxSprite;
    private var green_apple_texture:FlxSprite;

    override public function create():Void {
        super.create();

        doctor_texture = new FlxSprite(48, 42, AssetPaths.doctor_down__png);
        doctor_texture.setGraphicSize(32,32);

        add(doctor_texture);

        green_apple_texture = new FlxSprite(FlxG.width - 48, 42, AssetPaths.green_apple__png);
        green_apple_texture.setGraphicSize(32,32);

        add(green_apple_texture);

        game_name = new FlxText(0, 78, FlxG.width, "The old legends tell, \"An apple a day keeps the doctor away\","
            + " but this doctor sets out to prove, that you actually need a LOT of apples...", 16);
        game_name.setFormat(null, 16, FlxColor.WHITE, FlxTextAlign.CENTER);
       
        add(game_name);

        sub_text = new FlxText(0, FlxG.height - 32, FlxG.width,"Press 'SPACE' to start", 12);
        sub_text.setFormat(null, 12, FlxColor.WHITE, FlxTextAlign.CENTER);
        
        add(sub_text);
    }

    override public function update(elapsed:Float):Void {
        super.update(elapsed);
        if(FlxG.keys.justPressed.SPACE){
            FlxG.camera.fade(FlxColor.BLACK, 1.0, false, function (){
                FlxG.switchState(new PlayState());
            });
        }
    }
}