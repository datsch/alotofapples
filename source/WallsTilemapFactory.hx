package;

import flixel.FlxG;
import flixel.tile.FlxTilemap;
using flixel.addons.editors.ogmo.FlxOgmo3Loader;

class WallsTilemapFactory {
    private function new() {}

    public static function createWalls():FlxTilemap {
        var ogmoLoader = new FlxOgmo3Loader(AssetPaths.arena_map__ogmo, AssetPaths.arena__json);
        var walls = ogmoLoader.loadTilemap(AssetPaths.a_lot_of_apples_tileset__png, "walls");
        walls.x = (FlxG.width - walls.width)/2;
        return walls;
    }
}