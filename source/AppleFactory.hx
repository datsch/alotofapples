package;

class AppleFactory {
    private function new() {}

    public static function createApple(kindOfApple:String):Apple {
        switch (kindOfApple.toLowerCase()) {
            case "red": return new RedApple();
            case "green": return new GreenApple();
            case "yellow": return new YellowApple();
            case "blue": return new BlueApple();
            default: return new RedApple();
        }
    }
}