package;

interface IsViewControllable {
    public function isViewLeft():Bool;
    public function isViewRight():Bool;
    public function isViewUp():Bool;
    public function isViewDown():Bool;
}