package;

class BlueApple extends Apple {
    public function new() {
        super(AssetPaths.blue_apple__png);
    }
}