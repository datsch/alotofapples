package;

class GameVariables {
    public static var time:Float = 0.0;
    public static var applesToProduce: Int = 1;
    public static var timeOfRound(default, null):Float = 15;
    public static var nextProduceTime:Float=5;
}