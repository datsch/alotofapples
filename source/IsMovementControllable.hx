package;

interface IsMovementControllable {
    public function isMoveLeft():Bool;
    public function isMoveRight():Bool;
    public function isMoveUp():Bool;
    public function isMoveDown():Bool;
    public function isValidMovementCombo():Bool;
}