# A lot of apples

A game made for the Quarantine Jam on [itch.io](https://itch.io).
Link to game: [A lot of apples](https://datschy.itch.io/a-lot-of-apples)

All assets used under CC0.